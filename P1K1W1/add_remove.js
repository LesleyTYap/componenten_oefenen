// Hier koppel ik de opties aan de mogelijke keuzes die daarbij horen voor het tweetal dropdown menu's.
var grouping = {
    'Header': ['header1','header2','header3'],
    'Main': ['main1','main2','main3'],
    'Aside': ['aside1','aside2','aside3'],
    'Footer': ['footer1','footer2','footer3']
};

var frames = {
    'header1':[''],
    'header2':[''],
    'header3':[''],
    'main1':[''],
    'main2':[''],
    'main3':[''],
    'aside1':[''],
    'aside2':[''],
    'aside3':[''],
    'footer1':[''],
    'footer2':[''],
    'footer3':['']
};

// als er word geklikt op de .add button, dan word er een nieuwe rij in de tabel aangemaakt.
$(document).ready(function () {
    $(document).on('click','.add', function () {
        var html = '';
        html += '<tr>';
        html += '<td><select class="add_options dropdown_item" name="categorie_dropdown[]" required>' +
            '<option value="" disabled selected>Maak een keuze</option>' +
            '<option name="header" value="Header">Header</option>' +
            '<option name="main" value="Main">Main</option>' +
            '<option name="aside" value="Aside">Aside</option>' +
            '<option name="footer" value="Footer">Footer</option>' +
            '</select></td>';

        // toevoegen van tweede dropdown waar later bepaald word wat erin komt te staan.
        html += '<td><select class="add_choices dropdown_item" name="component_dropdown[]" required>' +
            '<option value="" disabled selected>Kies een component</option>' +
            '</select></td>';

        // Bij elke <tr> die toegevoegd word, word er ook een button toegevoegd die die <tr> ook weer kan verwijderen.
        html += '<td><button type="button" class="remove"  name="remove">-</button></td></tr>';



        $('.item_table').append(html);


        //Wanneer er een keuze in de eerste dropdown is veranderd, moeten de mogelijke opties opgezocht worden.
        $('.add_options').on('change',function () {

            var selectValue = $(this).val();
            var choiceSelect = $(this).parent().next().find('.add_choices');
            choiceSelect.empty();



            // haalt de mogelijke keuzes op uit var grouping en plaatst ze in de <option>
            for (i = 0; i < grouping[selectValue].length; i++){
                choiceSelect.append("<option value=' "+ grouping[selectValue][i] +"'>" + grouping[selectValue][i] + "</option>");
            }

        });


    });

    // Als er op de .remove button word geklikt, verwijder dan die <tr> waar op de button geklikt word.
    $(document).on('click','.remove',function () {
        $(this).closest('tr').remove();
    });
});





