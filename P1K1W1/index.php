<html>
<head>
    <meta charset="UTF-8">
    <title>B1K2W1</title>
    <link href="resources/css/reset.css" type="text/css" rel="stylesheet">
    <link href="resources/css/styling.css" type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>
<body>
<img class="goodday_logo" src="resources/img/logo.png">
<div class="container">
    <div class="title_container">
        <h1>Welkom!</h1>
        <h2>Stel hieronder zelf uw pagina samen.</h2>
    </div>


    <form action="test.php" method="post">
    <div class="dropdown_container">
        <table class="item_table content_table">
            <tr>
                <th>Component</th>
                <th>Keuze</th>
                <th><button type="button" name="add" class="add">+</button></th>
            </tr>
            <td>
                <select id="options" name="categorie_dropdown[]" class="dropdown_item" required>
                    <option value="" disabled selected>Maak een keuze</option>
                    <option name="header" value="Header">Header</option>
                    <option name="main" value="Main">Main</option>
                    <option name="aside" value="Aside">Aside</option>
                    <option name="footer" value="Footer">Footer</option>
                </select>
            </td>
            <td>
                <select id="choices" name="component_dropdown[]" class="dropdown_item" required>
                    <option class="testen" value="" disabled selected>Kies een component</option>
                </select>
            </td>
        </table>
    </div>


        <div class="iframe_container">
<!--            <iframe src="resources/templates/header1.html" scrolling="no"></iframe>-->
<!--            <iframe src="resources/templates/main1.html" scrolling="no"></iframe>-->
<!--            <iframe src="resources/templates/aside1.html" scrolling="no"></iframe>-->
<!--            <iframe src="resources/templates/footer1.html" scrolling="no"></iframe>-->
        </div>

    <div class="button_container">
        <input type="submit" class="submit_button">
    </div></form>

</div>
<script src="dependency.js"></script>
<script src="add_remove.js"></script>
</body>
</html>
