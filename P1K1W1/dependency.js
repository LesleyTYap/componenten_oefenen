// Hier koppel ik de opties aan de mogelijke keuzes die daarbij horen voor het tweetal dropdown menu's.
var grouping = {
    'Header': ['header1','header2','header3'],
    'Main': ['main1','main2','main3'],
    'Aside': ['aside1','aside2','aside3'],
    'Footer': ['footer1','footer2','footer3']
};

//Wanneer er een keuze in de eerste dropdown is veranderd, moeten de mogelijke opties opgezocht worden.
$(document).ready(function () {
    $('#options').on('change', function () {

        var selectValue = $(this).val();
        $('#choices').empty();

        for (i = 0; i < grouping[selectValue].length; i++) {
            $('#choices').append("<option value=' " + grouping[selectValue][i] + "'>" + grouping[selectValue][i] + "</option>");
        }

    });

    $('.add_choices').on('change', function () {
        var selectValue = $(this).val();
        var choiceSelect = $(this).parent().next().find('.iframe_container');
        choiceSelect.empty();

        for (i = 0; i < frames[selectValue].length; i++) {
            choiceSelect.append("<iframe value=' " + frames[selectValue][i] + "'>" + frames[selectValue][i] + "</iframe>");
        }

    });

    $(document).off('click', '.add').on('click', '.add', function () {
        var html = '';
        html += '<iframe></iframe>';
        console.log('test');
        $('.iframe_container').append(html);

        $('.add_choices').on('change', function () {
            var selectValue = $(this).val();
            var choiceSelect = $(this).parent().next().find('.iframe_container');
            choiceSelect.empty();

            for (i = 0; i < frames[selectValue].length; i++) {
                choiceSelect.append("<iframe class=' " + frames[selectValue][i] + "'>" + frames[selectValue][i] + "</iframe>");
            }

        });

        $(document).on('click', '.remove', function () {
            $('iframe').remove();
        });

    });
});

